#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import setup, find_packages
 
setup(
 
    # le nom de votre bibliothèque, tel qu'il apparaitra sur pypi
    name='sudoctools',
 
    # la version du code
    version="0.0.1",
 
    # Liste les packages à insérer dans la distribution
    # plutôt que de le faire à la main, on utilise la foncton
    # find_packages() de setuptools qui va cherche tous les packages
    # python recursivement dans le dossier courant.
    # C'est pour cette raison que l'on a tout mis dans un seul dossier:
    # on peut ainsi utiliser cette fonction facilement
    packages=find_packages(),
    author="Mathieu Saby",
    author_email="mathsabypro@gmail.com",
    description="Module expérimental",
    long_description=open('README.md').read(),
    install_requires= ["lxml>=4.3.4", "requests>=2.22.0"],
    # Active la prise en compte du fichier MANIFEST.in
    include_package_data=True,
 
    # Une url qui pointe vers la page officielle de votre lib
    url='http://gitlab.com',
 
    # Il est d'usage de mettre quelques metadata à propos de sa lib
    # Pour que les robots puissent facilement la classer.
    # La liste des marqueurs autorisées est longue:
    # https://pypi.python.org/pypi?%3Aaction=list_classifiers.
    #
    # Il n'y a pas vraiment de règle pour le contenu. Chacun fait un peu
    # comme il le sent. Il y en a qui ne mettent rien.
    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 1 - Planning",
        "License :: OSI Approved",
        "Natural Language :: French",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.6	",
        "Topic :: Libraries",
    ],
 
 
    # C'est un système de plugin, mais on s'en sert presque exclusivement
    # Pour créer des commandes, comme "django-admin".
    # Par exemple, si on veut créer la fabuleuse commande "proclame-sm", on
    # va faire pointer ce nom vers la fonction proclamer(). La commande sera
    # créé automatiquement. 
    # La syntaxe est "nom-de-commande-a-creer = package.module:fonction".
    entry_points = {
        'console_scripts': [
            'getsudocrecord = sudoctools.sudoctools:getSudocRecord',
            'issudocrecord = sudoctools.sudoctools:isSudocRecord',
            'getmergedsudocrecords = sudoctools.sudoctools:getMergedSudocRecords'
        ]
    },
 
    # A fournir uniquement si votre licence n'est pas listée dans "classifiers"
    # ce qui est notre cas
    license="WTFPL"
)
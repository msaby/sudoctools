#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Description du package.
"""
 
__version__ = "0.0.1"
 
from sudoctools.sudoctools import getSudocRecord, isSudocRecord, getMergedSudocRecords

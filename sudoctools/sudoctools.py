#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Description du module
    
    Usage:
    
    >> from sudoctools import getRecord
    >> getRecord()
"""

from lxml import etree
import requests
import logging
from typing import Optional

__all__ = ['getSudocRecord','isSudocRecord','getMergedSudocRecords' ]


log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)



def getSudocRecord (ppn : str) -> Optional[str]:        
    """Renvoit une notice du Sudoc en MARCXML.
    
    Wrapper autour du webservice UNIMARC/MARCXML (documentation : http://documentation.abes.fr/sudoc/manuels/administration/aidewebservices/index.html#SudocMarcXML)
    Si la notice n'existe pas, renvoit None.

    Parameters
    ----------
    ppn : str
        PPN de la notice recherchée
    
    Raises
    ------
    ValueError
        erreur HTTP (sauf erreur 404) ou renvoyée par le module requests

    Returns
    -------
    str
        Notice en MARCXML (ou None si la notice n'existe pas)
    """

    base_url = "https://www.sudoc.fr/"
    try:
        requete = requests.get(base_url+ppn+".xml")
        codehttp = requete.status_code
        if (codehttp == 404):
            log.debug ("Erreur 404 : le PPN {} n'existe pas".format (ppn))
            return None
        else:
            requete.raise_for_status()

    except requests.exceptions.HTTPError as errh:
        log.exception ("PPN {} : ERREUR HTTP {}".format (ppn, str(errh)))
        raise ValueError ("PPN {} : ERREUR HTTP {}".format (ppn, str(errh)))
        return None
    except requests.exceptions.ConnectionError as errc:
        log.exception ("PPN {} : ERREUR DE CONNEXION {}".format (ppn, str(errc)))
        raise ValueError ("PPN {} : ERREUR DE CONNEXION {}".format (ppn, str(errc))) 
        return None
    except requests.exceptions.Timeout as errt:
        log.exception ("PPN {} : ERREUR DE TIMEOUT {}".format (ppn, str(errt)))
        raise ValueError ("PPN {} : ERREUR DE TIMEOUT {}".format (ppn, str(errt)))
        return None
    except requests.exceptions.RequestException as errr:
        log.exception ("PPN {} : ERREUR {}".format (ppn, str(errr)))
        raise ValueError ("PPN {} : ERREUR {}".format (ppn, str(errr)))
        return None

    root = etree.XML(requete.content)
    if len (root.xpath("//error"))> 0:
        # normalement inutile car on aura une erreur 404 dans ce cas
        log.debug ("Erreur 404 : le PPN {} n'existe pas".format (ppn))
        return None
    return requete.content

def isSudocRecord (ppn : str) -> bool:      
    """Vérifie l'existence d'une notice dans le Sudoc.
    
    Parameters
    ----------
    ppn : str
        PPN de la notice recherchée
    
    Raises
    ------
    ValueError
        erreur renvoyée par la fonction getSudocRecord

    Returns
    -------
    bool
        True : la notice existe ; False : la notice n'existe pas
    """
    
    return (getSudocRecord(ppn) is not None)


def webserviceSudocMerged (*args : str) -> str:
    """Renvoit le résultat du webservice merged
    
    Documentation http://documentation.abes.fr/sudoc/manuels/administration/aidewebservices/#merged
    
    Parameters
    ----------
    *args : str
        Un ou plusieurs PPN à tester

    Raises
    ------
    ValueError
        erreur HTTP (sauf erreur 404) ou renvoyée par le module requests

    Returns
    -------
    str
        réponse de l'API en XML
    """
    base_url = "https://www.sudoc.fr/"
    service_url = "services/merged/"
    params = ",".join(args)
    try:
        requete = requests.get(base_url+service_url+params)
        codehttp = requete.status_code
        if (codehttp == 404):
            log.debug ("Erreur 404 : le(s) PPN(s) {} existent".format (params))
        else:
            requete.raise_for_status()

    except requests.exceptions.HTTPError as errh:
        log.exception ("PPN(s) {} : ERREUR HTTP {}".format (params, str(errh)))
        raise ValueError ("PPN(s) {} : ERREUR HTTP {}".format (params, str(errh)))
        return None
    except requests.exceptions.ConnectionError as errc:
        log.exception ("PPN(s) {} : ERREUR DE CONNEXION {}".format (params, str(errc)))
        raise ValueError ("PPN(s) {} : ERREUR DE CONNEXION {}".format (params, str(errc))) 
        return None
    except requests.exceptions.Timeout as errt:
        log.exception ("PPN(s) {} : ERREUR DE TIMEOUT {}".format (params, str(errt)))
        raise ValueError ("PPN(s) {} : ERREUR DE TIMEOUT {}".format (params, str(errt)))
        return None
    except requests.exceptions.RequestException as errr:
        log.exception ("PPN(s) {} : ERREUR {}".format (params, str(errr)))
        raise ValueError ("PPN(s) {} : ERREUR {}".format (params, str(errr)))
        return None
    return requete.content

def getMergedSudocRecords (*args : str,recursif=True) :
    """Renvoie une liste de réponses : ppn(s) cherché(s) -> ppn(s) trouvé(s) ou None"""

    apidata = webserviceSudocMerged (*args)
    root = etree.XML(apidata)
    error = root.xpath("//error")
    result = []
    if len(error) > 0:
        result = [[arg,None] for arg in args]
    else:
        queries = root.xpath("//query")
        # list : [[ppn1 cherché (string), liste de ppn trouvés (list)], [[ppn2 cherché (string), liste de ppn trouvés (list)]]
        result = [[query.xpath("./ppn/text()")[0],query.xpath("./result/ppn/text()")] for query in queries ]
        for r in result:
            if len(r[1]) == 1:
                # 1 seul PPN trouvé
                r [1] = r[1][0]
            else:
                # si pas récursif on renvoit la liste des ppn candidats telle quelle
                if recursif:
                # plusieurs PPN candidats, il faut les tester pour isoler le bon
                    test_candidats = getMergedSudocRecords (*r[1],recursif=False)
                    liste_candidats = []
                    for candidat in test_candidats:
                        if candidat[1] == None:
                            liste_candidats.append(candidat[0])
                    if len (liste_candidats) == 1 :
                        r [1] = liste_candidats[0]
                    else:
                        # ça ne devrait pas se produire
                        r[1] = None
        for arg in args:
            if arg not in [r[0] for r in result]:
                result.append ([arg, None])
    return result

if __name__ == "__main__":
    print ("Module sudoctools. lire README.md pour les instructions")

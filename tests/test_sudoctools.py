from sudoctools.sudoctools import getMergedSudocRecords as getMergedSudocRecords
import unittest

class TestgetMergedRecords(unittest.TestCase):

    def test_1(self):
        self.assertEqual(getMergedSudocRecords("abc"), [["abc",None]], "Résultat attendu : [[""abc"",None]]")
    def test_2(self):
        self.assertEqual(getMergedSudocRecords("abc","def"), [["abc",None],["def",None]], "Résultat attendu : [[""abc"":None],[""def"":None]]")
    def test_3(self):
        # ppn existant encore dans le sudoc
        self.assertEqual(getMergedSudocRecords("002722011"), [["002722011",None]], "Résultat attendu : [[""002722011"",None]]")
    def test_4(self):
        # 2 ppn existant encore dans le sudoc
        self.assertEqual(getMergedSudocRecords("002722011", "007527446"), [["002722011",None],["007527446",None]], "Résultat attendu : [[""002722011"",None],[""007527446"",None]]")
    def test_5(self):
        # ppn fusionné -> 1 PPN -> 1 seule réponse
        self.assertEqual(getMergedSudocRecords("071860576"), [["071860576","007527446"]], "Résultat attendu : [[""071860576"":""007527446""]]")
    def test_6(self):
        # ppn fusionné -> 2 PPN -> 1 seule réponse + non PPN
        self.assertEqual(getMergedSudocRecords("071860576","abc"), [["071860576","007527446"],["abc",None]], "Résultat attendu : [[""071860576"",""007527446""],[""abc"",None]]")
    def test_7(self):
        # ppn fusionné -> 2 PPN -> 1 seule réponse +  PPN encore existant
        self.assertEqual(getMergedSudocRecords("071860576","002722011"), [["071860576","007527446"],["002722011",None]], "Résultat attendu : [[""071860576"",""007527446""],[""002722011"",None]]")
    def test_8(self):
        # ppn fusionné -> 1 PPN -> 2 réponses
        self.assertEqual(getMergedSudocRecords("020250223"), [["020250223","002722011"]], "Résultat attendu : [[""020250223"",""002722011""]]")


if __name__ == "__main__":
    unittest.main()
    # a mettre dans une fonction
    try:
        result = getSudocRecord("002722011")
    except ValueError as e:
        print (e)
        result = None
    print (result)

    try:
        result = isSudocRecord("002722011")
    except ValueError as e:
        print (e)
        result = None
    print (result)